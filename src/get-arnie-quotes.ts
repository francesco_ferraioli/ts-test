import { httpGet } from './mock-http-interface';

type RawResponse = { status: number, body: string };

type ParsedResponse = { status: number, message: string };

type TResult = TSuccess | TFailure

type TSuccess = {
  "Arnie Quote": string
}

function buildSuccess(message: string): TSuccess {
  return {
    "Arnie Quote": message
  }
}

type TFailure = {
  "FAILURE": string
}

function buildFailure(message: string): TFailure {
  return {
    "FAILURE": message
  }
}

function parseResponse({status, body}: RawResponse): ParsedResponse {
  return {
    status,
    message: JSON.parse(body).message
  }
}

function responseToResult({status, message}: ParsedResponse): TResult {
  return status === 200 ? buildSuccess(message): buildFailure(message)
}

export const getArnieQuotes = async (urls : string[]) : Promise<TResult[]> => {
  const responses: RawResponse[] = await Promise.all(urls.map(httpGet))
  return responses.map(parseResponse).map(responseToResult)
};
